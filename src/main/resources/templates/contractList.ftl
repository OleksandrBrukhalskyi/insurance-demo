<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>Contracts List</h3>
<br>
<div>
    <table border="2">
        <tr>
            <th>Id</th>
            <th>Date</th>
            <th>AmountPay</th>
            <th>BasyTaryfPay</th>
            <th>Filiation</th>
            <th>InsuranceType</th>
            <th>Agent</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
        <#list contracts as contract>
            <tr>
                <td>${contract.id}</td>
                <td>${contract.date}</td>
                <td>${contract.amountPay}</td>
                <td>${contract.baseTaryfPay}</td>
                <td>${contract.filiation.name}</td>
                <td>${contract.insuranceType.name!"Null"}</td>
                <td>${contract.agent.surname}</td>
                <td><a href="/contract/delete/${contract.id}">Delete</a></td>
                <td><a href="/contract/edit/${contract.id}">Edit</a></td>
            </tr>
        </#list>
    </table>
    <a href="/contract/add/">Add Contract</a>
</div>
</body>
</html>