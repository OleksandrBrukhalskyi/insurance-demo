<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>

<div>
    <fieldset>
        <legend>Add filiation</legend>
        <form name="filiation" action="" method="POST">
            Name:<@spring.formInput "filiationForm.name" "" "text"/>
            <br>
            Address:<@spring.formInput "filiationForm.address" "" "text"/>
            <br>
            Phone:<@spring.formInput "filiationForm.phone" "" "text"/>
            <br>
            <input type="submit" value="Create"/>
        </form>
    </fieldset>
</div>

</body>
</html>