<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>

<div>
    <fieldset>
        <legend>Add agent</legend>
        <form name="agent" action="" method="POST">
            Surname:<@spring.formInput "agentForm.surname" "" "text"/>
            <br>
            Firstname:<@spring.formInput "agentForm.firstname" "" "text"/>
            <br>
            Patronymic:<@spring.formInput "agentForm.patronymic" "" "text"/>
            <br>
            Address:<@spring.formInput "agentForm.address" "" "text"/>
            <br>
            Phone:<@spring.formInput "agentForm.phone" "" "text"/>
            <br>
            Filiation:<@spring.formSingleSelect "agentForm.filiation", mavs, ""/>
            <br>
            <input type="submit" value="Create"/>
        </form>
    </fieldset>
</div>

</body>
</html>