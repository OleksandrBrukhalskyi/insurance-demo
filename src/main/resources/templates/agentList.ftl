<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>Agent List</h3>
<br>
<div>
    <table border="2">
        <tr>
            <th>Id</th>
            <th>Surname</th>
            <th>Firstname</th>
            <th>Patronymic</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Filiation</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
        <#list agents as agent>
        <tr>
            <td>${agent.id}</td>
            <td>${agent.surname}</td>
            <td>${agent.firstname}</td>
            <td>${agent.patronymic}</td>
            <td>${agent.address}</td>
            <td>${agent.phone}</td>
            <td>${agent.filiation}</td>
            <td><a href="/agent/delete/${agent.id}">Delete</a></td>
            <td><a href="/agent/edit/${agent.id}">Edit</a></td>
        </tr>
        </#list>
    </table>
    <a href="/agent/add/">Add agent</a>
</div>
</body>
</html>