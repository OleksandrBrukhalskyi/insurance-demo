<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<h3>Filiation List</h3>
<br>
<div>
    <table border="2">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Delete</th>
            <th>Edit</th>
        </tr>
        <#list filiations as filiation>
            <tr>
                <td>${filiation.id}</td>
                <td>${filiation.name}</td>
                <td>${filiation.address}</td>
                <td>${filiation.phone}</td>
                <td><a href="/filiation/delete/${filiation.id}">Delete</a></td>
                <td><a href="/filiation/edit/${filiation.id}">Edit</a></td>q
            </tr>
        </#list>
    </table>
    <a href="/filiation/add">Add Filiation</a>
</div>
</body>
</html>