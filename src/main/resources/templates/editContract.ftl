<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet"
          type="text/css" href="<@spring.url '/css/style.css'/>"/>
</head>
<body>
<div>
    <fieldset>
        <legend>Add Contract</legend>
        <form name="contract" action="" method="POST">
            Date:<@spring.formInput "contractForm.date" "" "date"/>
            <br>
            AmountPay:<@spring.formInput "contractForm.amountPay" "" "number"/>
            <br>
            BaseTaryfPay:<@spring.formInput "contractForm.baseTaryfPay" "" "number"/>
            <br>
            Filiation:<@spring.formSingleSelect "contractForm.filiation" ,mavs, "text"/>
            <br>
            InsuranceType:<@spring.formSingleSelect "contractForm.insuranceType" ,mavs2, "text"/>
            <br>
            Agent:<@spring.formSingleSelect "contractForm.agent" ,mavs3, "text"/>
            <input type="submit" value="Create"/>
        </form>
    </fieldset>
</div>
</body>
</html>