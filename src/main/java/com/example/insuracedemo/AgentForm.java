package com.example.insuracedemo;

import com.example.insuracedemo.model.Filiation;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class AgentForm {


        private String id;
        private String surname;
        private String firstname;
        private String patronymic;
        private String address;
        private String phone;
        private Filiation filiation;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getPatronymic() {
            return patronymic;
        }

        public void setPatronymic(String patronymic) {
            this.patronymic = patronymic;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Filiation getFiliation() {
            return filiation;
        }

        public void setFiliation(Filiation filiation) {
            this.filiation = filiation;
        }


    }

