package com.example.insuracedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuraceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsuraceDemoApplication.class, args);
    }

}
