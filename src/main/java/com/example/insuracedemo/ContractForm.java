package com.example.insuracedemo;

import com.example.insuracedemo.model.Agent;
import com.example.insuracedemo.model.Filiation;
import com.example.insuracedemo.model.InsuranceType;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

public class ContractForm {


        private String id;
        private String date;
        private double amountPay;
        private double baseTaryfPay;
        private Filiation filiation;
        private InsuranceType insuranceType;
        private Agent agent;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public double getAmountPay() {
            return amountPay;
        }

        public void setAmountPay(double amountPay) {
            this.amountPay = amountPay;
        }

        public double getBaseTaryfPay() {
            return baseTaryfPay;
        }

        public void setBaseTaryfPay(double baseTaryfPay) {
            this.baseTaryfPay = baseTaryfPay;
        }

        public Filiation getFiliation() {
            return filiation;
        }

        public void setFiliation(Filiation filiation) {
            this.filiation = filiation;
        }

        public InsuranceType getInsuranceType() {
            return insuranceType;
        }

        public void setInsuranceType(InsuranceType insuranceType) {
            this.insuranceType = insuranceType;
        }

        public Agent getAgent() {
            return agent;
        }

        public void setAgent(Agent agent) {
            this.agent = agent;
        }
    }


