package com.example.insuracedemo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class InsuranceType {
    @Id
    private String id;
    private String name;
    private double agentPercent;

    public InsuranceType(String id, String name, double agentPercent) {
        this.id = id;
        this.name = name;
        this.agentPercent = agentPercent;
    }

    public InsuranceType() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAgentPercent() {
        return agentPercent;
    }

    public void setAgentPercent(double agentPercent) {
        this.agentPercent = agentPercent;
    }

    @Override
    public String toString() {
        return "InsuranceType{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", agentPercent=" + agentPercent +
                '}';
    }
}
