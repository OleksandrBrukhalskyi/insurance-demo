package com.example.insuracedemo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Document
public class Contract {
    @Id
    private String id;
    private String date;
    private double amountPay;
    private double baseTaryfPay;
    private Filiation filiation;
    private InsuranceType insuranceType;
    private Agent agent;

    public Contract(String id, String date, double amountPay, double baseTaryfPay, Filiation filiation, InsuranceType insuranceType, Agent agent) {
        this.id = id;
        this.date = date;
        this.amountPay = amountPay;
        this.baseTaryfPay = baseTaryfPay;
        this.filiation = filiation;
        this.insuranceType = insuranceType;
        this.agent = agent;
    }

    public Contract() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getAmountPay() {
        return amountPay;
    }

    public void setAmoutPay(double amoutPay) {
        this.amountPay = amoutPay;
    }

    public double getBaseTaryfPay() {
        return baseTaryfPay;
    }

    public void setBaseTaryfPay(double baseTaryfPay) {
        this.baseTaryfPay = baseTaryfPay;
    }

    public Filiation getFiliation() {
        return filiation;
    }

    public void setFiliation(Filiation filiation) {
        this.filiation = filiation;
    }

    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(InsuranceType insuranceType) {
        this.insuranceType = insuranceType;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
}
