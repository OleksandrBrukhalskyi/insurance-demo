package com.example.insuracedemo.model;

import com.example.insuracedemo.service.InsuranceCompanyService;

public class InsuranceCompany {
    private static InsuranceCompany instance = null;

    private String name;
    private InsuranceCompanyService service;


    private InsuranceCompany() {
        service = new InsuranceCompanyService();
    }

    public static InsuranceCompany getInstance()
    {
        if (instance == null)
            instance = new InsuranceCompany();
        return instance;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public InsuranceCompanyService getService()
    {
        return service;
    }
//
//    public double getAgentSalary(String id, int year, int month)
//    {
//        double salary = 0;
//        for (Contract con: this.getService().getContracts())
//            if (con.getAgent().getId() == id &&
//                    con.getDate().getYear() == year &&
//                    con.getDate().getMonthValue() == month)
//                salary += con.getAmountPay() * con.getBaseTaryfPay() * con.getType().getAgentPercent();
//        return salary;
//    }
//    public double getIncomeOfFilliationPerMonth(String id, int year, int month)
//    {
//        double income = 0;
//        for (Contract con: this.getService().getContracts())
//            if (con.getFiliation().getId() == id && con.getDate().getYear() == year && con.getDate().getMonthValue() == month)
//                income += con.getAmountPay() * con.getBaseTaryfPay() * con.getType().getAgentPercent();
//        return income;
//    }
}
