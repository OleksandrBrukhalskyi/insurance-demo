package com.example.insuracedemo.repository;

import com.example.insuracedemo.model.Agent;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AgentRepository extends MongoRepository<Agent,String> {

}
