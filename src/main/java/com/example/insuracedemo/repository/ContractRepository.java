package com.example.insuracedemo.repository;

import com.example.insuracedemo.model.Contract;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ContractRepository extends MongoRepository<Contract,String> {
}
