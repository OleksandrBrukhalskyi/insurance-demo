package com.example.insuracedemo.repository;

import com.example.insuracedemo.model.InsuranceType;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InsuranceTypeRepository extends MongoRepository<InsuranceType,String> {
}
