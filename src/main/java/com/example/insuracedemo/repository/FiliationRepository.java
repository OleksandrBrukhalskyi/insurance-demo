package com.example.insuracedemo.repository;

import com.example.insuracedemo.model.Filiation;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FiliationRepository extends MongoRepository<Filiation,String> {
}
