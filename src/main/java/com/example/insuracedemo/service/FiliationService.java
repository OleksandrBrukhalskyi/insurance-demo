package com.example.insuracedemo.service;

import com.example.insuracedemo.model.Agent;
import com.example.insuracedemo.model.Filiation;
import com.example.insuracedemo.repository.FiliationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FiliationService {
    @Autowired
    FiliationRepository filiationRepository;

//    private List<Filiation> filiations = new ArrayList<>(
//            Arrays.asList(
//                    new Filiation("1","Filiation1","Holovna221","2243-1231"),
//                    new Filiation("2","Filiation2","Soborna Sqr12","2111-1232")
//            )
//    );
//    @PostConstruct
//    void init(){
//        this.filiationRepository.saveAll(filiations);
//    }
    public List<Filiation> getAll(){
        return  this.filiationRepository.findAll();
    }

    public Filiation create(Filiation filiation){
        return this.filiationRepository.save(filiation);
    }
    public Filiation update(Filiation filiation){
        return this.filiationRepository.save(filiation);
    }
    public void delete(String id) {
        this.filiationRepository.deleteById(id);
    }
    public Filiation getById(String id){
        return this.filiationRepository.findById(id).get();
    }
}
