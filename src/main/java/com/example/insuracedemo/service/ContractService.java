package com.example.insuracedemo.service;

import com.example.insuracedemo.model.Agent;
import com.example.insuracedemo.model.Contract;
import com.example.insuracedemo.model.Filiation;
import com.example.insuracedemo.model.InsuranceType;
import com.example.insuracedemo.repository.AgentRepository;
import com.example.insuracedemo.repository.ContractRepository;
import com.example.insuracedemo.repository.FiliationRepository;
import com.example.insuracedemo.repository.InsuranceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ContractService {
    @Autowired
    ContractRepository contractRepository;
    @Autowired
    FiliationService filiationService;
    @Autowired
    InsuranceTypeService insuranceTypeService;
    @Autowired
    AgentService agentService;

   private List<Contract> contracts = new ArrayList<>();

//   @PostConstruct
//    void  init(){
//        Filiation filiation1= filiationService.getAll().get(0);
//        Filiation filiation2=filiationService.getAll().get(1);
//
//        Agent agent1=agentService.getAll().get(0);
//        //Agent agent2=agentService.getAll().get(1);
//
//        InsuranceType insuranceType1=insuranceTypeService.getAll().get(0);
//        InsuranceType insuranceType2=insuranceTypeService.getAll().get(1);
//
//        Contract contract1=new Contract(null, "2019-03-26",1500,450,filiation1,insuranceType1,agent1);
//       // Contract contract2=new Contract(null,LocalDate.of(2019,02,24),1000,235,filiation2,insuranceType2,agent2);
//
//        contracts.add(contract1);
//        //contracts.add(contract2);
//
//      this.contractRepository.saveAll(contracts);
//    }

    public List<Contract> getAll(){
        return this.contractRepository.findAll();
    }
    public Contract create(Contract contract){
        return this.contractRepository.save(contract);
    }
    public Contract update (Contract contract){
        return this.contractRepository.save(contract);
    }
    public void delete(String id){
        this.contractRepository.deleteById(id);
    }
    public Contract getById(String id){
        return this.contractRepository.findById(id).get();
    }


}
