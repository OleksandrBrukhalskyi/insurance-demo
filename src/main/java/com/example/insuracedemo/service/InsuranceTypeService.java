package com.example.insuracedemo.service;

import com.example.insuracedemo.model.InsuranceType;
import com.example.insuracedemo.repository.InsuranceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class InsuranceTypeService {

    @Autowired
    InsuranceTypeRepository insuranceTypeRepository;

    private List<InsuranceType> insuranceTypeList = new ArrayList<>(
            Arrays.asList(
                    new InsuranceType("1","Gold",0.25),
                    new InsuranceType("2","Platinum",0.44)
            )
    );
    @PostConstruct
    void init(){
        this.insuranceTypeRepository.saveAll(insuranceTypeList);
    }

    public List<InsuranceType> getAll(){
        return  this.insuranceTypeRepository.findAll();
    }
    public InsuranceType create(InsuranceType insuranceType){
        return this.insuranceTypeRepository.save(insuranceType);
    }
    public InsuranceType update(InsuranceType insuranceType){
        return  this.insuranceTypeRepository.save(insuranceType);
    }
    public void delete(String id){
        this.insuranceTypeRepository.deleteById(id);
    }
    public InsuranceType getById(String id){
        return this.insuranceTypeRepository.findById(id).get();
    }

}
