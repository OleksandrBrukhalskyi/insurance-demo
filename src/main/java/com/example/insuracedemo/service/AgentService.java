package com.example.insuracedemo.service;

import com.example.insuracedemo.model.Agent;
import com.example.insuracedemo.model.Filiation;
import com.example.insuracedemo.repository.AgentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AgentService {

    @Autowired
    AgentRepository agentRepository;

    @Autowired
    FiliationService filiationService;

    private List<Agent> agents = new ArrayList<>();

//   private List<Agent> agents = new ArrayList<>(
//            Arrays.asList(
//                    new Agent("3","Tarasiuk","Sergiy","Petrovych","Holovna St,11","123-456","1"),
//                    new Agent("4","Petrenko","Nazar","Ihorovych","Nezalezhnosti Ave,115","456564","1")
//            )
//    );
//    @PostConstruct
//    void init(){
//
//        Filiation filiation1 =  filiationService.getAll().get(0);
//        Filiation filiation2 = filiationService.getAll().get(1);
//
//        Agent agent1 = new Agent(null,"Tarasiuk","Sergiy","Petrovych","Holovna St,11","123-456",filiation1);
//       Agent agent2 = new Agent(null,"Petrenko","Nazar","Ihorovych","Nezalezhnosti Ave,115","456564",filiation2);
//
//       agents.add(agent1);
//       agents.add(agent2);
//
//       this.agentRepository.saveAll(agents);
//
//  }

    public List<Agent> getAll(){
        return  this.agentRepository.findAll();
    }

    public Agent create(Agent agent){
        return this.agentRepository.save(agent);
    }
    public Agent update(Agent agent){
        return this.agentRepository.save(agent);
    }
    public void delete(String id) {
        this.agentRepository.deleteById(id);
    }
    public Agent getById(String id){
        return this.agentRepository.findById(id).get();
    }




}
