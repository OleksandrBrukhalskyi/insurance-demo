package com.example.insuracedemo.controller.web;

import com.example.insuracedemo.AgentForm;
import com.example.insuracedemo.model.Agent;
import com.example.insuracedemo.model.Filiation;
import com.example.insuracedemo.service.AgentService;
import com.example.insuracedemo.service.FiliationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class AgentController {


    @Autowired
    AgentService agentService;
    @Autowired
    FiliationService filiationService;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("message", "Hello world!");
        return "index";
    }

    @RequestMapping(value = "/agent/list",method = RequestMethod.GET)
    public String agentList(Model model){
        model.addAttribute("agents",agentService.getAll());
        return "agentList";
    }

    @RequestMapping(value = "/agent/add",method = RequestMethod.GET)
    public String addAgent(Model model){
        AgentForm agentForm = new AgentForm();
        Map<String,String> mavs = filiationService.getAll().stream()
                .collect(Collectors.toMap(Filiation::getId,Filiation::getName));
        model.addAttribute("mavs",mavs);

        model.addAttribute("agentForm",agentForm);
        return "addAgent";

    }
    @RequestMapping(value = "/agent/add",method = RequestMethod.POST)
    public String addAgent(Model model,
                           @ModelAttribute("agentForm") AgentForm agentForm){

        Agent newAgent = new Agent(agentForm.getId(),agentForm.getSurname(),agentForm.getFirstname(),agentForm.getPatronymic(),
                agentForm.getAddress(),agentForm.getPhone(),agentForm.getFiliation());
        agentService.create(newAgent);
        model.addAttribute("agents",agentService.getAll());
        return "agentList";
    }

    @RequestMapping(value = "/agent/edit/{id}",method = RequestMethod.GET)
    public String editAgent(Model model, @PathVariable("id") String id){
        Agent a = agentService.getById(id);

        Map<String,String> mavs =filiationService.getAll().stream()
                .collect(Collectors.toMap(Filiation::getId,Filiation::getName));

        AgentForm agentForm = new AgentForm();

        agentForm.setId(a.getId());
        agentForm.setSurname(a.getSurname());
        agentForm.setFirstname(a.getFirstname());
        agentForm.setPatronymic(a.getPatronymic());
        agentForm.setAddress(a.getAddress());
        agentForm.setPhone(a.getPhone());
        agentForm.setFiliation(a.getFiliation());

        model.addAttribute("mavs",mavs);
        model.addAttribute("agentForm",agentForm);
        return "editAgent";

    }
    @RequestMapping(value = "/agent/edit/{id}",method = RequestMethod.POST)
    public String editAgent(Model model,
                            @ModelAttribute("agentForm") AgentForm agentForm,
                            @PathVariable("id") String id){
        Agent a = new Agent();

        a.setId(agentForm.getId());
        a.setSurname(agentForm.getSurname());
        a.setFirstname(agentForm.getFirstname());
        a.setPatronymic(agentForm.getPatronymic());
        a.setAddress(agentForm.getAddress());
        a.setPhone(agentForm.getPhone());
        a.setFiliation(agentForm.getFiliation());
        agentService.update(a);
        model.addAttribute("agentForm",agentForm);
        return "agentList";

    }
    @RequestMapping(value = "/agent/delete/{id}",method = RequestMethod.GET)
    public String deleteAgent(Model model, @PathVariable("id") String id){
        agentService.delete(id);
        model.addAttribute("agents",agentService.getAll());
        return "agentList";
    }



}
