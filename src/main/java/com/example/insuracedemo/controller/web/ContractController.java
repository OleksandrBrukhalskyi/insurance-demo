package com.example.insuracedemo.controller.web;

import com.example.insuracedemo.ContractForm;
import com.example.insuracedemo.model.Agent;
import com.example.insuracedemo.model.Contract;
import com.example.insuracedemo.model.Filiation;
import com.example.insuracedemo.model.InsuranceType;
import com.example.insuracedemo.repository.AgentRepository;
import com.example.insuracedemo.service.AgentService;
import com.example.insuracedemo.service.ContractService;
import com.example.insuracedemo.service.FiliationService;
import com.example.insuracedemo.service.InsuranceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class ContractController {


    @Autowired
    ContractService contractService;


    @Autowired
    AgentService agentService;
    @Autowired
    InsuranceTypeService insuranceTypeService;
    @Autowired
    FiliationService filiationService;


    @RequestMapping(value = "/contract/list",method = RequestMethod.GET)
    public String contractList(Model model){
        model.addAttribute("contracts",contractService.getAll());
        System.out.println(contractService.getAll());
        return "contractList";
    }

    @RequestMapping(value = "/contract/add",method = RequestMethod.GET)
    public String addContract(Model model){

        ContractForm contractForm = new ContractForm();

        Map<String,String> mavs = filiationService.getAll().stream()
                .collect(Collectors.toMap(Filiation::getId,Filiation::getName));
        Map<String,String> mavs2=insuranceTypeService.getAll().stream()
                .collect(Collectors.toMap(InsuranceType::getId,InsuranceType::getName));
        Map<String,String> mavs3=agentService.getAll().stream()
                .collect(Collectors.toMap(Agent::getId,Agent::getSurname));


        model.addAttribute("mavs",mavs);
        model.addAttribute("mavs2",mavs2);
        model.addAttribute("mavs3",mavs3);
        model.addAttribute("contractForm",contractForm);

        return "addContract";
    }
    @RequestMapping(value = "/contract/add",method = RequestMethod.POST)
    public String addContract(Model model,
                              @ModelAttribute("contractForm") ContractForm contractForm){

        Contract newContract = new Contract(contractForm.getId(),contractForm.getDate(),
                contractForm.getAmountPay(),contractForm.getBaseTaryfPay(),contractForm.getFiliation()
                ,contractForm.getInsuranceType(),contractForm.getAgent());
        contractService.create(newContract);
        model.addAttribute("contracts",contractService.getAll());
        return "contractList";
    }

    @RequestMapping(value = "/contract/edit/{id}",method = RequestMethod.GET)
    public String editContract(Model model,
                               @PathVariable("id") String id){
        Contract con = contractService.getById(id);


        Map<String,String> mavs = filiationService.getAll().stream()
                .collect(Collectors.toMap(Filiation::getId,Filiation::getName));
        Map<String,String> mavs2=insuranceTypeService.getAll().stream()
                .collect(Collectors.toMap(InsuranceType::getId,InsuranceType::getName));
        Map<String,String> mavs3=agentService.getAll().stream()
                .collect(Collectors.toMap(Agent::getId,Agent::getSurname));

        ContractForm contractForm =new ContractForm();

        contractForm.setId(con.getId());
        contractForm.setAgent(con.getAgent());
        contractForm.setDate(con.getDate());
        contractForm.setAmountPay(con.getAmountPay());
        contractForm.setBaseTaryfPay(con.getBaseTaryfPay());
        contractForm.setFiliation(con.getFiliation());
        contractForm.setInsuranceType(con.getInsuranceType());
        model.addAttribute("mavs",mavs);
        model.addAttribute("mavs2",mavs2);
        model.addAttribute("mavs3",mavs3);
        model.addAttribute("contractForm",contractForm);
        return "editContract";//contractList

    }
    @RequestMapping(value = "/contract/edit/{id}",method = RequestMethod.POST)
    public String editContract(Model model,
                               @ModelAttribute("contractForm") ContractForm contractForm,
                               @PathVariable("id") String id){
        Contract c = new Contract();

        c.setId(contractForm.getId());
        c.setAgent(contractForm.getAgent());
        c.setDate(contractForm.getDate());
        c.setBaseTaryfPay(contractForm.getBaseTaryfPay());
        c.setInsuranceType(contractForm.getInsuranceType());
        c.setFiliation(contractForm.getFiliation());
        c.setAmoutPay(contractForm.getAmountPay());
        contractService.update(c);
        model.addAttribute("contracts",contractService.getAll());
        return "contractList";
    }

    @RequestMapping(value = "/contract/delete/{id}",method = RequestMethod.GET)
    public String deleteContract(Model model,@PathVariable("id")String id){
        contractService.delete(id);
        model.addAttribute("contracts",contractService.getAll());
        return "contractList";
    }

}
