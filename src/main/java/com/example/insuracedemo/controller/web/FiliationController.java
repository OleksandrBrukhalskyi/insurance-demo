package com.example.insuracedemo.controller.web;

import com.example.insuracedemo.FiliationForm;
import com.example.insuracedemo.model.Filiation;
import com.example.insuracedemo.service.FiliationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Controller
public class FiliationController {
    @Autowired
    FiliationService filiationService;

    @RequestMapping(value = "/filiation/list",method = RequestMethod.GET)
    public String filiationList(Model model){
        model.addAttribute("filiations",filiationService.getAll());
        return "filiationList";
    }

    @RequestMapping(value = "/filiation/add",method = RequestMethod.GET)
    public String addFiliation(Model model){
        FiliationForm filiationForm = new FiliationForm();
        model.addAttribute("filiationForm",filiationForm);
        return "addFiliation";
    }

    @RequestMapping(value = "/filiation/add",method = RequestMethod.POST)
    public String addFiliation(Model model,
                               @ModelAttribute("filiationForm") FiliationForm filiationForm){
        Filiation newFiliation = new Filiation(filiationForm.getId(),
                filiationForm.getName(),filiationForm.getAddress(),filiationForm.getPhone());
        filiationService.create(newFiliation);
        model.addAttribute("filiations",filiationService.getAll());
        return "filiationList";
    }
    @RequestMapping(value = "/filiation/edit/{id}",method = RequestMethod.GET)
    public String editFiliation(Model model, @PathVariable("id") String id){
        Filiation f = filiationService.getById(id);
        Map<String,String> mavs = filiationService.getAll().stream()
                .collect(Collectors.toMap(Filiation::getId,Filiation::getName));

        FiliationForm filiationForm = new FiliationForm();
        filiationForm.setId(f.getId());
        filiationForm.setName(f.getName());
        filiationForm.setAddress(f.getAddress());
        filiationForm.setPhone(f.getPhone());
        model.addAttribute("filiationForm",filiationForm);
        return "editFiliation";
    }
    @RequestMapping(value = "/filiation/edit/{id}",method = RequestMethod.POST)
    public String editFiliation(Model model,
                                @ModelAttribute("filiationForm")FiliationForm filiationForm,
                                @PathVariable("id") String id){
        Filiation filiation = new Filiation();
        filiation.setId(filiationForm.getId());
        filiation.setName(filiationForm.getName());
        filiation.setAddress(filiationForm.getAddress());
        filiation.setPhone(filiationForm.getPhone());
        filiationService.update(filiation);
        model.addAttribute("filiationForm",filiationForm);
        return "filiationList";

    }
    @RequestMapping(value = "/filiation/delete/{id}",method = RequestMethod.GET)
    public String deleteFiliation(Model model,@PathVariable("id") String id){
        filiationService.delete(id);
        model.addAttribute("filiationList",filiationService.getAll());
        return "filiationList";
    }
}
